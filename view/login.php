<?php $auth = 0;
include '../lib/includes.php';

// Formulaire de connexion
if (isset($_POST['username']) && isset($_POST['password'])) {
    login($_POST['username'], $_POST['password']);
    die();
}

include './templates/login_header.php';

?>
    <form action="#" method="post">
        <div class="form-group">
            <label for="username">Nom d'utilisateur</label>
            <?= input('username') ?>
        </div>
        <div class="form-group">
            <label for="password">Mot de passe</label>
            <input type="password" class="form-control" id="password" name="password">
        </div>
        <button type="submit" class="btn btn-outline-dark">Se connecter</button>
    </form>
