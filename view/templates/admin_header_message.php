<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Mon administration</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="../../resources/css/bootstrap.min.css">

    <!-- JQuery -->
    <script rel="stylesheet" src="../../resources/js/jquery.min.js"></script>
    <script rel="stylesheet" src="../../resources/js/bootstrap.min.js"></script>

    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="../../resources/css/mdb.min.css">

    <!-- Stepper -->
    <link href="../../resources/css/addons-pro/steppers.css" rel="stylesheet">
    <link href="../../resources/css/addons-pro/steppers.min.css" rel="stylesheet">
    <script type="text/javascript" src="../../resources/js/addons-pro/steppers.js"></script>
    <script type="text/javascript" src="../../resources/js/addons-pro/steppers.min.js"></script>

    <!-- Autres librairies -->
    <script src="https://kit.fontawesome.com/c41628d08b.js" crossorigin="anonymous"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

    <link rel="stylesheet" href="../../resources/css/style_admin.css">
</head>
<body>
<nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-dark" id="navbar-top">
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <ul class="navbar-nav mr-auto">
            <li><a class="nav-item nav-link" href="../admin/category.php">Catégories</a></li>
            <li><a class="nav-item nav-link" href="../admin/work.php">Réalisations</a></li>
            <li><a class="nav-item nav-link" href="../admin/internship.php">Stages</a></li>
            <li><a class="nav-item nav-link" href="../admin/skill.php">Compétences</a></li>
            <li><a class="nav-item nav-link" href="../admin/degree.php">Formations</a></li>
            <li><a class="nav-item nav-link" href="../admin/contact.php">Messages</a></li>
        </ul>
        <div class="navbar-nav">
            <a class="nav-item nav-link" href="../logout.php">Déconnexion</a>
        </div>
    </div>
</nav>
<nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="navbar-top-small">
    <div class="pos-f-t">
        <div class="collapse" id="navbarToggleExternalContent">
            <ul class="navbar-nav mr-auto">
                <li><a class="nav-item nav-link" href="../admin/category.php">Catégories</a></li>
                <li><a class="nav-item nav-link" href="../admin/work.php">Réalisations</a></li>
                <li><a class="nav-item nav-link" href="../admin/internship.php">Stages</a></li>
                <li><a class="nav-item nav-link" href="../admin/skill.php">Compétences</a></li>
                <li><a class="nav-item nav-link" href="../admin/degree.php">Formations</a></li>
                <li><a class="nav-item nav-link" href="../admin/contact.php">Messages</a></li>
            </ul>
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="../logout.php">Déconnexion</a>
            </div>
        </div>
        <nav class="navbar navbar-dark bg-dark">
            <button class="navbar-toggler" type="button" data-toggle="collapse"
                    data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent"
                    aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </nav>
    </div>
</nav>

<div>
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>

<?= flash(); ?>