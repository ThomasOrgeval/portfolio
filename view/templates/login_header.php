<!DOCTYPE html>
<html lang="fr">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">
    <title>Thomas Orgeval - Portfolio</title>

    <!-- Bootstrap -->
    <link rel="stylesheet" href="../resources/css/bootstrap.min.css">

    <!-- JQuery -->
    <script rel="stylesheet" src="../resources/js/jquery.min.js"></script>
    <script rel="stylesheet" src="../resources/js/bootstrap.min.js"></script>

    <!-- Material Design Bootstrap -->
    <link rel="stylesheet" href="../resources/css/mdb.min.css">

    <!-- Stepper -->
    <link href="../resources/css/addons-pro/steppers.css" rel="stylesheet">
    <link href="../resources/css/addons-pro/steppers.min.css" rel="stylesheet">
    <script type="text/javascript" src="../resources/js/addons-pro/steppers.js"></script>
    <script type="text/javascript" src="../resources/js/addons-pro/steppers.min.js"></script>

    <!-- Autres librairies -->
    <script src="https://kit.fontawesome.com/c41628d08b.js" crossorigin="anonymous"></script>
    <script src='https://www.google.com/recaptcha/api.js'></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

    <link rel="stylesheet" href="../resources/css/style.css">
</head>
<body>
<nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark">
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <ul class="navbar-nav mr-auto">
            <li class="nav-item">
                <a class="nav-item nav-link" href="<?= WEBROOT; ?>index.php">Accueil</a>
            </li>
        </ul>
        <div class="navbar-nav">
            <a class="nav-item nav-link" href="<?= WEBROOT; ?>view/login.php">Administration</a>
        </div>
    </div>
</nav>
<div class="container">
    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>

<?= flash(); ?>