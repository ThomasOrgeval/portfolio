</div>

<nav class="navbar navbar-expand-sm navbar-dark bg-dark footer">
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="mr-auto">

        </div>
        <ul class="nav navbar-nav" id="items-footer">
            <li class="nav-item">
                <a class="btn btn-social-icon btn-footer"
                   href="https://bitbucket.org/%7B52b60173-7df8-4bf1-929c-04a686c2fb95%7D/" target=_blank>
                    <span class="fa fa-bitbucket" style="font-size: 40px; color: #ffffff;"></span>
                </a>
            </li>
            <li>
                <a class="btn btn-social-icon btn-footer"
                   href="https://www.linkedin.com/in/thomas-orgeval-a58a31171/" target=_blank>
                    <span class="fa fa-linkedin" style="font-size: 40px; color: #ffffff;"></span>
                </a>
            </li>
            <li>
                <a class="btn btn-social-icon btn-footer"
                   href="https://codepen.io/thomasorgeval" target=_blank>
                    <span class="fa fa-codepen" style="font-size: 40px; color: #ffffff;"></span>
                </a>
            </li>
        </ul>
    </div> <!-- close the container tag -->
</nav> <!-- close the nav tag -->

<?php if (isset($script)) : ?><?= $script; ?><?php endif; ?>

</body>
</html>
