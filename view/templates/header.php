<?php
setcookie("portfolio");
$_COOKIE['hauteur'] = $_COOKIE['hauteur'] - 56;

?>
    <!DOCTYPE html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">
        <title>Thomas Orgeval - Portfolio</title>

        <!-- Bootstrap -->
        <link rel="stylesheet" href="resources/css/bootstrap.min.css">

        <!-- JQuery -->
        <script rel="stylesheet" src="resources/js/jquery.min.js"></script>
        <script rel="stylesheet" src="resources/js/bootstrap.min.js"></script>

        <!-- Material Design Bootstrap -->
        <link rel="stylesheet" href="resources/css/mdb.min.css">

        <!-- Stepper -->
        <link href="resources/css/addons-pro/steppers.css" rel="stylesheet">
        <link href="resources/css/addons-pro/steppers.min.css" rel="stylesheet">
        <script type="text/javascript" src="resources/js/addons-pro/steppers.js"></script>
        <script type="text/javascript" src="resources/js/addons-pro/steppers.min.js"></script>

        <!-- Autres librairies -->
        <script src="https://kit.fontawesome.com/c41628d08b.js" crossorigin="anonymous"></script>
        <script src='https://www.google.com/recaptcha/api.js'></script>
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
        <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>

        <link rel="stylesheet" href="resources/css/style.css">
    </head>
    <body>

    <div>
        <img class="card-img-top" id="image-top" src="resources/image/header-bg.jpg"
             style="width: 100%; height: <?= $_COOKIE ['hauteur']; ?>px;">
        <img class="card-img-top" id="image-top-small" src="resources/image/header-bg-small.jpg">
        <!--div class="card-img-overlay">
            <h5 class="card-title">Thomas Orgeval</h5>
        </div-->
    </div>

    <nav class="navbar sticky-top navbar-expand-lg navbar-dark bg-dark " id="navbar-top">
        <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
            <ul class="navbar-nav mr-auto">
                <li class="nav-item"><a class="nav-link active" href="#">Accueil</a></li>
                <li class="nav-item"><a class="nav-link" href="#section1">Qui suis-je ?</a></li>
                <li class="nav-item"><a class="nav-link" href="#section2">Compétences</a></li>
                <li class="nav-item"><a class="nav-link" href="#section3">Portfolio</a></li>
                <li class="nav-item"><a class="nav-link" href="#section4">Expériences</a></li>
                <li class="nav-item"><a class="nav-link" href="#section5">Formation</a></li>
                <li class="nav-item"><a class="nav-link" href="#section6">Contact</a></li>
            </ul>
            <div class="navbar-nav">
                <a class="nav-item nav-link" href="<?= WEBROOT; ?>view/login.php">Administration</a>
            </div>
        </div>
    </nav>

    <nav class="navbar navbar-expand-lg navbar-dark bg-dark" id="navbar-top-small">
        <div class="pos-f-t">
            <div class="collapse" id="navbarToggleExternalContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item"><a class="nav-link active" href="#">Accueil</a></li>
                    <li class="nav-item"><a class="nav-link" href="#section1">Qui suis-je ?</a></li>
                    <li class="nav-item"><a class="nav-link" href="#section2">Compétences</a></li>
                    <li class="nav-item"><a class="nav-link" href="#section3">Portfolio</a></li>
                    <li class="nav-item"><a class="nav-link" href="#section4">Expériences</a></li>
                    <li class="nav-item"><a class="nav-link" href="#section5">Formation</a></li>
                    <li class="nav-item"><a class="nav-link" href="#section6">Contact</a></li>
                </ul>
                <div class="navbar-nav">
                    <a class="nav-item nav-link" href="<?= WEBROOT; ?>view/login.php">Administration</a>
                </div>
            </div>
            <nav class="navbar navbar-dark bg-dark">
                <button class="navbar-toggler" type="button" data-toggle="collapse"
                        data-target="#navbarToggleExternalContent" aria-controls="navbarToggleExternalContent"
                        aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>
            </nav>
        </div>
    </nav>

    <script>
        document.cookie = "hauteur=" + window.innerHeight + "; expires=0";
        /*window.onload = function() {
            if(!window.location.hash) {
                window.location = window.location + '#loaded';
                window.location.reload();
            }
        }*/
    </script>

    </body>
<?= flash(); ?>