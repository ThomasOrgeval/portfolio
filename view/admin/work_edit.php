<?php
include "../../lib/includes.php";

$db = connexiondb();

/***
 * Sauvegarde
 */
if (isset($_POST['name']) && isset($_POST['slug'])) {
    checkCsrf();
    if (preg_match('/^[a-z\-0-9]+$/', $_POST['slug'])) {
        $images = array();
        createRea();
        header("Location:work.php");
        die();
    } else {
        setFlash("Le slug n'est pas valide", 'danger');
    }
}

/***
 * Redirection si l'id n'existe pas
 */
if (isset($_GET['id'])) {
    if (redirectRea()) {
        header("Location:work.php");
        die();
    }
}

/***
 * Liste des catégories pour la création d'une expérience
 */
$categories = listeCateg();
$categories_list = array();
foreach ($categories as $category) {
    $categories_list[$category['id']] = $category['name'];
}
/***
 * Liste des images
 */
if (isset($_GET['id'])) {
    $images = listeImages();
} else {
    $images = array();
}

/***
 * Suppression d'une image
 */
if (isset($_GET['delete_image'])) {
    checkCsrf();
    $image = deleteImage();
    header('Location:work_edit.php?id=' . $image['experience_id']);
    die();
}

/***
 * Mise en avant d'une image
 */
if (isset($_GET['highlight_image'])) {
    checkCsrf();
    miseEnAvant();
    header('Location:work_edit.php?id=' . $_GET['id']);
    die();
}

include '../templates/admin_header.php';

?>

    <h1 class="h1-admin">Editer une expérience</h1>
    <p>&nbsp;</p>

    <div class="row">
        <form action="#" method="post" enctype="multipart/form-data" style="width: 100%;
                                                                            display: flex;">
            <div class="col-sm-8 small-screen">
                <div class="form-group">
                    <label for="name">Nom de l'expérience</label>
                    <?= input('name'); ?>
                </div>
                <div class="form-group">
                    <label for="slug">URL de l'expérience</label>
                    <?= input('slug'); ?>
                </div>
                <div class="form-group">
                    <label for="content">Contenu de l'expérience</label>
                    <?= textarea('content'); ?>
                </div>
                <div class="form-group">
                    <label for="category_id">Catégorie</label>
                    <?= select('category_id', $categories_list); ?>
                </div>

                <!- Affiachage sur mobile ->

                <div class="add-image-small">
                    <div class="list-group">
                        <input type="file" name="images[]">
                        <input type="file" name="images[]" class="invisible" id="duplicate">
                    </div>
                    <p><a href="#" class="btn btn-outline-success" id="duplicatebtn">Ajouter une image</a></p>
                    <?php foreach ($images as $k => $image): ?>
                        <p>
                            <img src="../../resources/image/exp/<?= $image['name']; ?>" width="150">
                            <a href="?delete_image=<?= $image['id']; ?>&<?= csrf(); ?>"
                               onclick="return confirm('Etes vous sur ?');">Supprimer</a>
                            <a href="?highlight_image=<?= $image['id']; ?>&id=<?= $_GET['id']; ?>&<?= csrf(); ?>">Mettre
                                à la une</a>
                        </p>
                    <?php endforeach ?>
                </div>


                <?= csrfInput(); ?>
                <p class="add">
                    <button type="submit" class="btn btn-outline-dark">Enregistrer</button>
                </p>
            </div>

            <!- Grands écrans ->

            <div class="col-sm-4 add-image">
                <div class="list-group">
                    <input type="file" name="images[]">
                    <input type="file" name="images[]" class="invisible" id="duplicate2">
                </div>
                <p>
                    <a href="#" class="btn btn-outline-success" id="duplicatebtn2">Ajouter une image</a>
                </p>
                <?php foreach ($images as $k => $image): ?>
                    <p>
                        <img src="../../resources/image/exp/<?= $image['name']; ?>" width="150">
                        <a href="?delete_image=<?= $image['id']; ?>&<?= csrf(); ?>"
                           onclick="return confirm('Etes vous sur ?');">Supprimer</a>
                        <a href="?highlight_image=<?= $image['id']; ?>&id=<?= $_GET['id']; ?>&<?= csrf(); ?>">Mettre à
                            la une</a>
                    </p>
                <?php endforeach ?>
            </div>
        </form>
    </div>


<?php ob_start(); ?>

    <script src="https://cdn.tiny.cloud/1/cq9fpx7h3gak9curb21o4hzf8745n6g3yyrzor7w98mm93hn/tinymce/5/tinymce.min.js"
            referrerpolicy="origin"></script>
    <script src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>

    <script>
        (function ($) {
            $('#duplicatebtn').click(function (e) {
                e.preventDefault();
                var $clone = $('#duplicate').clone().attr('id', '').removeClass('invisible');
                $('#duplicate').before($clone);
            })
        })(jQuery);
        (function ($) {
            $('#duplicatebtn2').click(function (e) {
                e.preventDefault();
                var $clone = $('#duplicate2').clone().attr('id', '').removeClass('invisible');
                $('#duplicate2').before($clone);
            })
        })(jQuery);
    </script>

    <script>
        tinymce.init({
            selector: "textarea",
        });
    </script>

<?php $script = ob_get_clean();
include "../templates/admin_footer.php";
