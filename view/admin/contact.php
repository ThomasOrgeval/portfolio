<?php
include "../../lib/includes.php";

/**
 * Suppression
 */
if (isset($_GET['delete'])) {
    checkCsrf();
    $id = deleteContact();
    header('Location:contact.php');
}

/***
 *  Messages
 */
$contacts = contacts();

include '../templates/admin_header_message.php';
?>

    <h1 class="h1-admin-left">Mes messages</h1>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Nom</th>
            <th>Sujet</th>
            <th>Message</th>
            <th>Action</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($contacts as $contact): ?>
            <tr>
                <td><?= $contact['name']; ?></td>
                <td><?= $contact['subject']; ?></td>
                <td><?= $contact['message']; ?></td>
                <td>
                    <a href="?delete=<?= $contact['id']; ?>&<?= csrf(); ?>" class="btn btn-outline-danger"
                       onclick="return confirm('Voulez-vous vraiment supprimer ce message ?')">Remove</a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

<?php include "../templates/admin_footer.php";