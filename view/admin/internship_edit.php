<?php
include "../../lib/includes.php";


/***
 * Sauvegarde
 */
if (isset($_POST['name']) && isset($_POST['slug'])) {
    checkCsrf();
    if (preg_match('/^[a-z\-0-9]+$/', $_POST['slug'])) {
        createIntern();
        header("Location:internship.php");
        die();
    } else {
        setFlash("Le slug n'est pas valide", 'danger');
    }
}

/***
 * Redirection si l'id n'existe pas
 */
if (isset($_GET['id'])) {
    if (redirectIntern()) {
        header("Location:internship.php");
        die();
    }
}

include '../templates/admin_header.php'; ?>

<h1 class="h1-admin">Editer un stage</h1>

<form action="#" method="post">
    <div class="form-group">
        <label for="name">Nom du stage</label>
        <?= input('name'); ?>
    </div>
    <div class="form-group">
        <label for="slug">URL du stage</label>
        <?= input('slug'); ?>
    </div>
    <div class="form-group">
        <label for="content">Contenu du stage</label>
        <?= textarea('content'); ?>
    </div>
    <div class="form-group">
        <label for="slug">Date de début</label>
        <?= input('date_debut'); ?>
    </div>
    <div class="form-group">
        <label for="slug">Date de fin</label>
        <?= input('date_fin'); ?>
    </div>
    <?= csrfInput(); ?>
    <button type="submit" class="btn btn-outline-dark">Enregistrer</button>
</form>

<?php ob_start(); ?>

    <script src="https://cdn.tiny.cloud/1/cq9fpx7h3gak9curb21o4hzf8745n6g3yyrzor7w98mm93hn/tinymce/5/tinymce.min.js"
            referrerpolicy="origin"></script>

    <script>
        tinymce.init({
            selector: "textarea",
        });
    </script>

<?php $script = ob_get_clean();
include "../templates/admin_footer.php";