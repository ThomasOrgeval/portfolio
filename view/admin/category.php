<?php
include "../../lib/includes.php";
include '../templates/admin_header.php';

/**
 * Suppression
 */
if (isset($_GET['delete'])) {
    checkCsrf();
    $id = deleteCateg();
    header('Location:category.php');
}

/**
 * Catégories
 */
$categories = categories();
?>

    <h1 class="h1-admin-left">Les catégories</h1>

    <p class="add"><a href="category_edit.php" class="btn btn-success">Ajout</a></p>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Id</th>
            <th>Nom</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($categories as $category): ?>
            <tr>
                <td><?= $category['id']; ?></td>
                <td><?= $category['name']; ?></td>
                <td>
                    <a href="category_edit.php?id=<?= $category['id']; ?>" class="btn btn-outline-dark">Edit</a>
                    <a href="?delete=<?= $category['id']; ?>&<?= csrf(); ?>" class="btn btn-outline-danger"
                       onclick="return confirm('Voulez-vous vraiment supprimer cette catégorie ?')">Remove</a>
                </td>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>

<?php include "../templates/admin_footer.php";
