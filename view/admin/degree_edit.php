<?php
include "../../lib/includes.php";

/***
 * Sauvegarde
 */
if (isset($_POST['name']) && isset($_POST['slug'])) {
    checkCsrf();
    if (preg_match('/^[a-z\-0-9]+$/', $_POST['slug'])) {
        createDegree();
        header("Location:degree.php");
        die();
    } else {
        setFlash("Le slug n'est pas valide", 'danger');
    }
}

/***
 * Redirection si l'id n'existe pas
 */
if (isset($_GET['id'])) {
    if (redirectDegree()) {
        header("Location:degree.php");
        die();
    }
}

include '../templates/admin_header.php'; ?>

    <h1 class="h1-admin">Editer un diplôme</h1>

    <form action="#" method="post">
        <div class="form-group">
            <label for="name">Nom de la formation</label>
            <?= input('name'); ?>
        </div>
        <div class="form-group">
            <label for="slug">URL de la formation</label>
            <?= input('slug'); ?>
        </div>
        <div class="form-group">
            <label for="adresse">Ecole de la formation</label>
            <?= input('adresse'); ?>
        </div>
        <div class="form-group">
            <label for="options">Options de la formation</label>
            <?= input('options'); ?>
        </div>
        <div class="form-group">
            <label for="content">Contenu de la formation</label>
            <?= textarea('content'); ?>
        </div>
        <div class="form-group">
            <label for="annee">Année d'obtention du diplôme (prévisualisation)</label>
            <?= input('annee'); ?>
        </div>
        <?= csrfInput(); ?>
        <button type="submit" class="btn btn-outline-dark">Enregistrer</button>
    </form>

<?php ob_start(); ?>

    <script src="https://cdn.tiny.cloud/1/cq9fpx7h3gak9curb21o4hzf8745n6g3yyrzor7w98mm93hn/tinymce/5/tinymce.min.js"
            referrerpolicy="origin"></script>

    <script>
        tinymce.init({
            selector: "textarea",
        });
    </script>

<?php $script = ob_get_clean();
include "../templates/admin_footer.php";