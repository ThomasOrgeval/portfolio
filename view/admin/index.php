<?php
include "../../lib/includes.php";
include '../templates/admin_header.php'; ?>
    <h1 class="h1-admin">Mon administration</h1>
    <p>&nbsp;</p>
    <div class="row-cols-1 display">
        <a class="btn btn-outline-dark btn-admin" href="../admin/category.php">Catégories</a>
        <a class="btn btn-outline-dark btn-admin" href="../admin/work.php">Réalisations</a>
        <a class="btn btn-outline-dark btn-admin" href="../admin/internship.php">Stages</a>
        <a class="btn btn-outline-dark btn-admin" href="../admin/skill.php">Compétences</a>
        <a class="btn btn-outline-dark btn-admin" href="../admin/degree.php">Formations</a>
        <a class="btn btn-outline-dark btn-admin" href="../admin/contact.php">Messages</a>
    </div>
<?php include "../templates/admin_footer.php";

