<?php
include "../../lib/includes.php";
include '../templates/admin_header.php';

/**
 * Suppression
 */
if (isset($_GET['delete'])) {
    checkCsrf();
    $id = deleteDegree();
    header('Location:degree.php');
}

/**
 * Compétences
 */
$degrees = degrees();
?>

    <h1 class="h1-admin-left">Les Formations</h1>

    <p class="add"><a href="degree_edit.php" class="btn btn-success">Ajout</a></p>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Id</th>
            <th>Nom</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($degrees as $degree): ?>
            <tr>
                <td><?= $degree['id']; ?></td>
                <td><?= $degree['name']; ?></td>
                <td>
                    <a href="degree_edit.php?id=<?= $degree['id']; ?>" class="btn btn-outline-dark">Edit</a>
                    <a href="?delete=<?= $degree['id']; ?>&<?= csrf(); ?>" class="btn btn-outline-danger"
                       onclick="return confirm('Voulez-vous vraiment supprimer ce diplôme ?')">Remove</a>
                </td>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>

<?php include "../templates/admin_footer.php";
