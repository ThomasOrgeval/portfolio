<?php
include "../../lib/includes.php";

/***
 * Sauvegarde
 */
if (isset($_POST['name']) && isset($_POST['slug'])) {
    checkCsrf();
    $db = connexiondb();
    if (preg_match('/^[a-z\-0-9]+$/', $_POST['slug'])) {
        $name = $db->quote($_POST['name']);
        $slug = $db->quote($_POST['slug']);
        if (isset($_GET['id'])) {
            $id = $db->quote($_GET['id']);
            $db->query("update categories set name=$name, slug=$slug where id=$id");
        } else {
            $db->query("insert into categories set name=$name, slug=$slug");
        }
        setFlash("La catégorie a bien été ajoutée");
        header("Location:category.php");
        die();
    } else {
        setFlash("Le slug n'est pas valide", 'danger');
    }
}

/***
 * Redirection si l'id n'existe pas
 */
if (isset($_GET['id'])) {
    $db = connexiondb();
    $id = $db->quote($_GET['id']);
    $select = $db->query("select * from categories where id=$id");
    if ($select->rowCount() == 0) {
        setFlash("Il n'y a pas de catégorie avec cet ID", "danger");
        header("Location:category.php");
        die();
    }
    $_POST = $select->fetch();
}

include '../templates/admin_header.php';

?>

    <h1 class="h1-admin">Editer une catégorie</h1>

    <form action="#" method="post">
        <div class="form-group">
            <label for="name">Nom de la catégorie</label>
            <?= input('name'); ?>
        </div>
        <div class="form-group">
            <label for="slug">URL de la catégorie</label>
            <?= input('slug'); ?>
        </div>
        <?= csrfInput(); ?>
        <button type="submit" class="btn btn-outline-dark">Enregistrer</button>
    </form>


<?php include "../templates/admin_footer.php";