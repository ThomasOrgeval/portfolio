<?php
include "../../lib/includes.php";


/***
 * Sauvegarde
 */
if (isset($_POST['name']) && isset($_POST['slug'])) {
    checkCsrf();
    if (preg_match('/^[a-z\-0-9]+$/', $_POST['slug'])) {
        createSkill();
        header("Location:skill.php");
        die();
    } else {
        setFlash("Le slug n'est pas valide", 'danger');
    }
}

/***
 * Redirection si l'id n'existe pas
 */
if (isset($_GET['id'])) {
    if (redirectSkill()) {
        header("Location:skill.php");
        die();
    }
}

include '../templates/admin_header.php'; ?>

    <h1 class="h1-admin">Editer une compétence</h1>

    <form action="#" method="post">
        <div class="form-group">
            <label for="name">Nom de la compétence</label>
            <?= input('name'); ?>
        </div>
        <div class="form-group">
            <label for="slug">URL de la compétence</label>
            <?= input('slug'); ?>
        </div>
        <div class="form-group">
            <label for="valeur">Valeur de la compétence</label>
            <?= input('valeur'); ?>
        </div>
        <?= csrfInput(); ?>
        <button type="submit" class="btn btn-outline-dark">Enregistrer</button>
    </form>

<?php include "../templates/admin_footer.php";