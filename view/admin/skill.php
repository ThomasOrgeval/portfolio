<?php
include "../../lib/includes.php";
include '../templates/admin_header.php';

/**
 * Suppression
 */
if (isset($_GET['delete'])) {
    checkCsrf();
    $id = deleteSkill();
    header('Location:skill.php');
}

/**
 * Compétences
 */
$skills = skills();
?>

    <h1 class="h1-admin-left">Les compétences</h1>

    <p class="add"><a href="skill_edit.php" class="btn btn-success">Ajout</a></p>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Id</th>
            <th>Nom</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($skills as $skill): ?>
            <tr>
                <td><?= $skill['id']; ?></td>
                <td><?= $skill['name']; ?></td>
                <td>
                    <a href="skill_edit.php?id=<?= $skill['id']; ?>" class="btn btn-outline-dark">Edit</a>
                    <a href="?delete=<?= $skill['id']; ?>&<?= csrf(); ?>" class="btn btn-outline-danger"
                       onclick="return confirm('Voulez-vous vraiment supprimer cette compétence ?')">Remove</a>
                </td>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>

<?php include "../templates/admin_footer.php";
