<?php
include "../../lib/includes.php";

/***
 * Suppression
 */
if (isset($_GET['delete'])) {
    checkCsrf();
    $id = deleteIntern();
    header('Location:internship.php');
}

/***
 * Stages
 */
$internships = internship();

include '../templates/admin_header.php';
?>

    <h1 class="h1-admin-left hauteur">Les stages et expériences professionnelles </h1>

    <p class="add"><a href="internship_edit.php" class="btn btn-success">Ajout</a></p>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Id</th>
            <th>Nom</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($internships as $internship): ?>
            <tr>
                <td><?= $internship['id']; ?></td>
                <td><?= $internship['name']; ?></td>
                <td>
                    <a href="internship_edit.php?id=<?= $internship['id']; ?>" class="btn btn-outline-dark">Edit</a>
                    <a href="?delete=<?= $internship['id']; ?>&<?= csrf(); ?>" class="btn btn-outline-danger"
                       onclick="return confirm('Voulez-vous vraiment supprimer ce stage ?')">Remove</a>
                </td>
            </tr>
        <?php endforeach; ?>
        </tbody>
    </table>

<?php include "../templates/admin_footer.php";