<?php
include "../../lib/includes.php";
include '../templates/admin_header.php';

/***
 * Suppression
 */
if (isset($_GET['delete'])) {
    checkCsrf();
    $id = deleteRea();
    header("Location:work.php");
}

/***
 * Réalisations
 */
$realisations = realisations();
?>

    <h1 class="h1-admin-left">Les projets et expériences professionnelles</h1>

    <p class="add"><a href="work_edit.php" class="btn btn-success">Ajout</a></p>

    <table class="table table-striped">
        <thead>
        <tr>
            <th>Id</th>
            <th>Nom</th>
            <th>Actions</th>
        </tr>
        </thead>
        <tbody>
        <?php foreach ($realisations as $realisation): ?>
            <tr>
                <td><?= $realisation['id']; ?></td>
                <td><?= $realisation['name']; ?></td>
                <td>
                    <a href="work_edit.php?id=<?= $realisation['id']; ?>" class="btn btn-outline-dark">Edit</a>
                    <a href="?delete=<?= $realisation['id']; ?>&<?= csrf(); ?>" class="btn btn-outline-danger"
                       onclick="return confirm('Voulez-vous vraiment supprimer cette expérience ?')">Remove</a>
                </td>
            </tr>
        <?php endforeach ?>
        </tbody>
    </table>

<?php include "../templates/admin_footer.php";