<?php $auth = 0;
include "../lib/includes.php";

$db = connexiondb();

if (!isset($_GET['id'])) {
    header("HTTP/1.1 301 Moved Permanently");
    header("Location:" . WEBROOT . "/index.php");
    die();
}
$exp_id = $db->quote($_GET['id']);
$select = $db->query("select * from experiences where id=$exp_id");
if ($select->rowCount() == 0) {
    header("HTTP/1.1 301 Moved Permanently");
    header("Location:" . WEBROOT . "/index.php");
    die();
}
$experiences = $select->fetch();
$select = $db->query("select * from images where experience_id=$exp_id");
$images = $select->fetchAll();

include "./templates/login_header.php";
?>
    <h1><?= $experiences['name']; ?></h1>

<?= $experiences['content']; ?>

<?php foreach ($images as $k => $image): ?>
    <p>
        <img src="../resources/image/exp/<?= $image['name']; ?>" width="100%" alt="">
    </p>
<?php endforeach ?>

    <p>&nbsp;</p>
    <p>&nbsp;</p>
    <p>&nbsp;</p>

<?php include "./templates/footer.php";
