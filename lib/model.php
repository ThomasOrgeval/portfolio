<?php

function connexiondb()
{
    try {
        $db = new PDO('mysql:host=localhost;dbname=portfolio', 'root', '');
        $db->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_ASSOC);
        $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_WARNING); // Affiche toutes les alertes
    } catch (Exception $e) {
        echo 'Connexion impossible à la base de données';
        echo $e->getMessage(); // affiche le message d'erreur
        die();
    }
    return $db;
}

/***
 * Login administration
 */

function login($user, $pass)
{
    $db = connexiondb();
    $username = $db->quote($user);
    $password = sha1($pass);
    $select = $db->query("select * from user where username=$username and password='$password'");
    if ($select->rowCount() > 0) {
        $_SESSION['Auth'] = $select->fetch();
        setFlash('Vous êtes maintenant connecté');
        header('Location:' . WEBROOT . './view/admin/index.php');
        die();
    }
}

/***
 * Catégories
 */

function categories()
{
    $db = connexiondb();
    $select = $db->query("select id, name from categories");
    return $select->fetchAll();
}

function listeCateg()
{
    $db = connexiondb();
    $select = $db->query("select id, name from categories order by name asc");
    return $select->fetchAll();
}

function deleteCateg()
{
    $db = connexiondb();
    $id = $db->quote($_GET['delete']);
    $db->query("delete from categories where id=$id");
    setFlash('La catégorie a bien été supprimée');
    return $id;
}


/***
 * Réalisations
 */

function realisations()
{
    $db = connexiondb();
    $select = $db->query("select id, name from experiences");
    return $select->fetchAll();
}

function createRea()
{
    $db = connexiondb();
    $name = $db->quote($_POST['name']);
    $slug = $db->quote($_POST['slug']);
    $content = $db->quote($_POST['content']);
    $category_id = $db->quote($_POST['category_id']);

    // Sauvegarde expérience

    if (isset($_GET['id'])) {
        $id = $db->quote($_GET['id']);
        $db->query("update experiences set name=$name, slug=$slug, content=$content, category_id=$category_id where id=$id");
    } else {
        $db->query("insert into experiences set name=$name, slug=$slug, content=$content, category_id=$category_id");
        $_GET['id'] = $db->lastInsertId();
    }
    setFlash("L'expérience a bien été ajoutée");

    // Envois des images

    $exp_id = $db->quote($_GET['id']);
    $files = $_FILES['images'];
    foreach ($files['tmp_name'] as $k => $v) {
        $image = array(
            'name' => $files['name'][$k],
            'tmp_name' => $files['tmp_name'][$k]
        );
        $extension = pathinfo($image['name'], PATHINFO_EXTENSION);
        if (in_array($extension, array('jpg', 'png'))) {
            $db->query("insert into images set experience_id=$exp_id");
            $image_id = $db->lastInsertId();
            $image_name = $image_id . '.' . $extension;
            move_uploaded_file($image['tmp_name'], IMAGES . "/exp/" . $image_name);
            $image_name = $db->quote($image_name);
            $db->query("update images set name=$image_name where id=$image_id");
        }
    }
}

function deleteRea()
{
    $db = connexiondb();
    $id = $db->quote($_GET['delete']);
    $images = $db->query("select id, name from images where experience_id=$id");
    $db->query("update experiences set image_id=null where id=$id");
    foreach ($images as $k => $image) {
        unlink(IMAGES . '/exp/' . $image['name']);
    }
    $db->query("delete from images where experience_id=$id");
    $db->query("delete from experiences where id=$id");
    setFlash('L\'expérience a bien été supprimée');
    return $id;
}

function redirectRea()
{
    $db = connexiondb();
    $id = $db->quote($_GET['id']);
    $select = $db->query("select * from experiences where id=$id");
    if ($select->rowCount() == 0) {
        setFlash("Il n'y a pas d'expérience avec cet ID", "danger");
        return true;
    }
    $_POST = $select->fetch();
}


/***
 * Images
 */

function listeImages()
{
    $db = connexiondb();
    $exp_id = $db->quote($_GET['id']);
    $select = $db->query("select id, name from images where experience_id=$exp_id");
    return $select->fetchAll();
}

function deleteImage()
{
    $db = connexiondb();
    $id = $db->quote($_GET['delete_image']);
    $image = $db->query("select name, experience_id from images where id=$id")->fetch();
    $experience = $db->query("select * from experiences where image_id=$id")->fetch();
    $exp_id = $experience['id'];
    unlink(IMAGES . '/exp/' . $image['name']);
    $db->query("update experiences set image_id=null where id=$exp_id");
    $db->query("delete from images where id=$id");
    setFlash("L'image a bien été supprimée");
    return $image;
}

function miseEnAvant()
{
    $db = connexiondb();
    $exp_id = $db->quote($_GET['id']);
    $image_id = $db->quote($_GET['highlight_image']);
    $db->query("update experiences set image_id=$image_id where id=$exp_id");
    setFlash("L'image a bien été mise en avant");
}


/***
 * Stages
 */

function internship()
{
    $db = connexiondb();
    $select = $db->query("select id, name from internship");
    return $select->fetchAll();
}

function deleteIntern()
{
    $db = connexiondb();
    $id = $db->quote($_GET['delete']);
    $db->query("delete from internship where id=$id");
    setFlash('Le stage a bien été supprimé');
    return $id;
}

function createIntern()
{
    $db = connexiondb();
    $name = $db->quote($_POST['name']);
    $slug = $db->quote($_POST['slug']);
    $content = $db->quote($_POST['content']);
    $date_debut = $db->quote($_POST['date_debut']);
    $date_fin = $db->quote($_POST['date_fin']);

    if (isset($_GET['id'])) {
        $id = $db->quote($_GET['id']);
        $db->query("update internship set name=$name, slug=$slug, content=$content, date_debut=$date_debut, date_fin=$date_fin where id=$id");
    } else {
        $db->query("insert into internship set name=$name, slug=$slug, content=$content, date_debut=$date_debut, date_fin=$date_fin");
    }
    setFlash("Le stage a bien été ajoutée");
}

function redirectIntern()
{
    $db = connexiondb();
    $id = $db->quote($_GET['id']);
    $select = $db->query("select * from internship where id=$id");
    if ($select->rowCount() == 0) {
        setFlash("Il n'y a pas d'expérience avec cet ID", "danger");
        return true;
    }
    $_POST = $select->fetch();
}

function listeIntern()
{
    $db = connexiondb();
    $select = $db->query("select * from internship order by id desc");
    return $select->fetchAll();
}

/**
 * Compétences
 */

function skills()
{
    $db = connexiondb();
    $select = $db->query("select id, name from skill");
    return $select->fetchAll();
}

function listeSkills()
{
    $db = connexiondb();
    $select = $db->query("select name, valeur from skill order by valeur desc");
    return $select->fetchAll();
}

function deleteSkill()
{
    $db = connexiondb();
    $id = $db->quote($_GET['delete']);
    $db->query("delete from skill where id=$id");
    setFlash('La compétence a bien été supprimée');
    return $id;
}

function createSkill()
{
    $db = connexiondb();
    $name = $db->quote($_POST['name']);
    $slug = $db->quote($_POST['slug']);
    $valeur = $db->quote($_POST['valeur']);

    if (isset($_GET['id'])) {
        $id = $db->quote($_GET['id']);
        $db->query("update skill set name=$name, slug=$slug, valeur=$valeur where id=$id");
    } else {
        $db->query("insert into skill set name=$name, slug=$slug, valeur=$valeur");
    }
    setFlash("La compétence a bien été ajoutée");
}


function redirectSkill()
{
    $db = connexiondb();
    $id = $db->quote($_GET['id']);
    $select = $db->query("select * from skill where id=$id");
    if ($select->rowCount() == 0) {
        setFlash("Il n'y a pas de compétence avec cet ID", "danger");
        return true;
    }
    $_POST = $select->fetch();
}

/**
 * Formation
 */

function degrees()
{
    $db = connexiondb();
    $select = $db->query("select id, name from degree");
    return $select->fetchAll();
}

function listeDegrees()
{
    $db = connexiondb();
    $select = $db->query("select * from degree order by annee desc");
    return $select->fetchAll();
}

function deleteDegree()
{
    $db = connexiondb();
    $id = $db->quote($_GET['delete']);
    $db->query("delete from degree where id=$id");
    setFlash('Le diplôme a bien été supprimé');
    return $id;
}

function createDegree()
{
    $db = connexiondb();
    $name = $db->quote($_POST['name']);
    $slug = $db->quote($_POST['slug']);
    $adresse = $db->quote($_POST['adresse']);
    $options = $db->quote($_POST['options']);
    $content = $db->quote($_POST['content']);
    $annee = $db->quote($_POST['annee']);

    if (isset($_GET['id'])) {
        $id = $db->quote($_GET['id']);
        $db->query("update degree set name=$name, slug=$slug, adresse=$adresse, options=$options, content=$content, annee=$annee where id=$id");
    } else {
        $db->query("insert into degree set name=$name, slug=$slug, adresse=$adresse, options=$options, content=$content, annee=$annee");
    }
    setFlash("Le diplôme a bien été ajouté");
}


function redirectDegree()
{
    $db = connexiondb();
    $id = $db->quote($_GET['id']);
    $select = $db->query("select * from degree where id=$id");
    if ($select->rowCount() == 0) {
        setFlash("Il n'y a pas de diplôme avec cet ID", "danger");
        return true;
    }
    $_POST = $select->fetch();
}

/**
 * Contact
 */

function contacts()
{
    $db = connexiondb();
    $select = $db->query("select * from contact");
    return $select->fetchAll();
}

function createContact()
{
    $db = connexiondb();
    $name = $db->quote(htmlspecialchars($_POST['name']));
    $email = $db->quote(htmlspecialchars($_POST['email']));
    $subject = $db->quote(htmlspecialchars($_POST['subject']));
    $message = $db->quote(htmlspecialchars($_POST['message']));

    $db->query("insert into contact set name=$name, email=$email, subject=$subject, message=$message");
}

function deleteContact()
{
    $db = connexiondb();
    $id = $db->quote($_GET['delete']);
    $db->query("delete from contact where id=$id");
    setFlash('Le message a bien été supprimé');
    return $id;

}