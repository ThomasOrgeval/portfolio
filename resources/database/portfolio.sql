drop database if exists portfolio;
create database portfolio;
use portfolio;

create table `USER`
(
    `id`       int auto_increment not null,
    `username` varchar(255)       not null,
    `password` varchar(255)       not null,
    primary key (`id`)
) engine = InnoDB;

create table `CATEGORIES`
(
    `id`   int auto_increment not null,
    `name` varchar(255)       not null,
    `slug` varchar(255)       not null,
    primary key (`id`)
) engine = InnoDB;

create table `EXPERIENCES`
(
    `id`          int auto_increment not null,
    `name`        varchar(255)       not null,
    `slug`        varchar(255)       not null,
    `content`     longtext           not null,
    `category_id` int                not null,
    `image_id`    int,
    primary key (`id`)
) engine = InnoDB;

create table `IMAGES`
(
    `id`            int auto_increment not null,
    `name`          varchar(255),
    `experience_id` int                not null,
    primary key (`id`)
) engine = InnoDB;

create table `INTERNSHIP`
(
    `id`         int auto_increment not null,
    `name`       varchar(255)       not null,
    `slug`       varchar(255)       not null,
    `content`    longtext           not null,
    `date_debut` varchar(255)       not null,
    `date_fin`   varchar(255)       not null,
    primary key (`id`)
) engine = InnoDB;

create table `SKILL`
(
    `id`     int auto_increment not null,
    `name`   varchar(255)       not null,
    `slug`   varchar(255)       not null,
    `valeur` int                not null,
    primary key (`id`)
) engine = InnoDB;

create table `DEGREE`
(
    `id`      int auto_increment not null,
    `name`    varchar(255)       not null,
    `slug`    varchar(255)       not null,
    `adresse` varchar(255)       not null,
    `options` varchar(255)       not null,
    `content` longtext           not null,
    `annee`   int                not null,
    primary key (`id`)
) engine = InnoDB;

create table `CONTACT`
(
    `id`      int auto_increment not null,
    `name`    varchar(255)       not null,
    `email`   varchar(255)       not null,
    `subject` varchar(255)       not null,
    `message` longtext           not null,
    primary key (`id`)
);

alter table `EXPERIENCES`
    add constraint foreign key (`category_id`) references `CATEGORIES` (`id`);

alter table `EXPERIENCES`
    add constraint foreign key (`image_id`) references `IMAGES` (`id`);

alter table `IMAGES`
    add constraint foreign key (`experience_id`) references `EXPERIENCES` (`id`);


insert into `USER` (id, username, password)
values (1, 'raiwtsu', sha1('gj82zfh3q'));

insert into `CATEGORIES` (id, name, slug)
VALUES (1, 'Java', 'java'),
       (2, 'PHP', 'php'),
       (3, 'Keyhole Markup Language', 'kml');

insert into `EXPERIENCES` (id, name, slug, content, category_id, image_id)
VALUES (1, 'Portfolio', 'portfolio', 'Contenu1', '2', null),
       (2, 'Projet GSB - Application de gestion des visites', 'gsbvisite', 'Contenu2', '1', null),
       (3, 'Projet GSB - Site de gestion des fiches de frais', 'gsbfiches', 'Contenu3', '2', null),
       (4, 'Marqueurs Google Earth - B.E.S.', 'markbes', 'Contenu4', '3', null),
       (5, 'Kalicustomer - Application de gestion des congés payés', 'kalicustomer', 'Contenu5', '1', null);

insert into `IMAGES` (id, name, experience_id)
VALUES (1, '1.jpg', 1),
       (2, '2.png', 3),
       (3, '3.jpg', 2);

insert into `INTERNSHIP` (id, name, slug, content, date_debut, date_fin)
VALUES (1, 'Kalicustomer', 'kalicustomer', '<p class="MsoNormal" style="mso-pagination: none; mso-layout-grid-align: none; text-autospace: none; vertical-align: middle;">Application web en Java EE de satisfaction client</p>
<p class="MsoNormal" style="mso-pagination: none; mso-layout-grid-align: none; text-autospace: none; vertical-align: middle;">Application de gestion des congés payés</p>
<p class="MsoNormal" style="mso-pagination: none; mso-layout-grid-align: none; text-autospace: none; vertical-align: middle;">Création d''un composant (tableaux cliquables)</p>',
        'Mai 2019', 'Juin 2019'),
       (2, 'DataGeo/B.E.S.', 'datageo', '<p>Automatisation des t&acirc;ches</p>
<p>Réalisation d''applications pour le tri d''informations sur Excel, créations de marqueurs sur Google Earth</p>',
        'Janvier 2020', 'Février 2020'),
       (3, 'Bretagnes Etudes Services', 'bes', '<p>Script d''automatisation d''autocad afin d''exporter les données de chacun des objets présents<p>', 'Juin 2020', 'Aout 2020');

insert into `SKILL` (id, name, slug, valeur)
VALUES (1, 'Java - Java EE', 'java', 85),
       (2, 'HTML - CSS', 'html', 80),
       (3, 'MySQL', 'mysql', 75),
       (4, 'Python', 'python', 65),
       (5, 'PHP', 'php', 65),
       (6, 'JavaScript', 'javascript', 55),
       (7, 'VBA', 'vba', 80);

insert into `DEGREE` (id, name, slug, adresse, options, content, annee)
VALUES (1, 'Bac S', 'bacs', 'Kerneuzec, Quimperlé', 'Option mathématiques', '', 2018),
       (2, 'BTS SIO', 'btssio', 'Notre Dame de la Paix, Ploemeur', 'Option SLAM, mathématiques appronfondies', '',
        2020),
       (3, 'Licence L3 mention informatique', 'l3', 'Université de Rennes 1, Rennes', 'parcours MIAGE', '', 2021);

update `EXPERIENCES`
set image_id=1
where id = 1;
update `EXPERIENCES`
set image_id=2
where id = 3;
update `EXPERIENCES`
set image_id=3
where id = 2;