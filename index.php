<?php $auth = 0;
include "./lib/includes.php";

$db = connexiondb();

$experiences = $db->query("
    select experiences.name, experiences.id, experiences.slug, images.name as image_name 
    from experiences 
    left join images on images.id = experiences.image_id")->fetchAll();

$internships = listeIntern();
$skills = listeSkills();
$degrees = listeDegrees();

if (isset($_POST['name']) && isset($_POST['email'])) {
    if (!empty($_POST['name']) || !empty($_POST['email']) || !empty($_POST['subject']) || !empty($_POST['message'])) {
        createContact();
        header("Location:index.php");
        die();
    } else {
        // Formulaire de champs remplis a revoir et obligatoire
        setFlash("Il manque un champ !", "danger");
    }
}

$count = 1;
include "./view/templates/header.php";
?>
    <div class="scroll-container">
        <!- Section A propos ->

        <div id="section1" class="container-fluid">
            <h1 class="div-image-cv">Qui suis-je ? [NOT UPDATED]</h1>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <div class="container row-cols-2 small-screen flexible">
                <div class="col-sm-8 center-small">
                    <h3>Orgeval Thomas, 20 ans</h3>
                    <p>&nbsp;</p>
                    <p>Bienvenue sur mon portfolio ! Je me présente je me nomme Thomas Orgeval.</p>
                    <p>Beaucoup de choses me passionnent et il m'est malheureusement impossible de tout dire ici :(</p>
                    <p>Cependant vous pouvez trouver tout ce dont vous avez besoin concernant le développement sur moi !
                        :D</p>

                    <ul class="nav navbar-nav flexible bar-icons" id="items-footer">
                        <li class="size-icons">
                            <a class="btn btn-footer"
                               href="https://bitbucket.org/%7B52b60173-7df8-4bf1-929c-04a686c2fb95%7D/" target=_blank>
                                <span class="fa fa-bitbucket" style="font-size: 40px; color: #000;"></span>
                            </a>
                        </li>
                        <li class="size-icons">
                            <a class="btn btn-footer"
                               href="https://www.linkedin.com/in/thomas-orgeval-a58a31171/" target=_blank>
                                <span class="fa fa-linkedin" style="font-size: 40px; color: #000;"></span>
                            </a>
                        </li>
                        <li class="size-icons">
                            <a class="btn btn-footer"
                               href="https://codepen.io/thomasorgeval" target=_blank>
                                <span class="fa fa-codepen" style="font-size: 40px; color: #000;"></span>
                            </a>
                        </li>
                        <li class="size-icons">
                            <a class="btn btn-footer" href="CV.pdf" target="_blank">
                                <span class="fa fa-file-text-o" style="font-size: 40px; color: #000"></span>
                            </a>
                        </li>
                    </ul>

                </div>
                <div class="col-sm-1 div-image-cv">
                    <img src="resources/image/maPtiteTete.jpg" class="image-cv rounded" alt="">
                </div>
            </div>
        </div>

        <!- Section Copmétences ->

        <div id="section2" class="container-fluid black">
            <h1 style="text-align: center" class="white-text">Compétences en développement</h1>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <div class="container row-cols-1">
                <div>
                    <?php foreach ($skills as $item => $skill): ?>
                        <h3 style="margin-top: 10px"><?= $skill['name']; ?></h3>
                        <div class="progress md-progress">
                            <div class="progress-bar" role="progressbar" style="width: <?= $skill['valeur']; ?>%;"
                                 aria-valuenow="0" aria-valuemin="0" aria-valuemax="100"></div>
                        </div>
                    <?php endforeach; ?>
                </div>
            </div>
        </div>

        <!- Section Portfolio ->

        <div id="section3" class="container-fluid">
            <h1 style="text-align: center">Portfolio</h1>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <div class="container row-cols-1 flexible">

                <div id="carousel" class="carousel slide carousel-fade" data-ride="carousel">

                    <!-- Indicators -->
                    <ul class="carousel-indicators">
                        <?php foreach ($experiences as $item => $experience):
                            if ($experience['id'] == 1): ?>
                                <li data-target="#carousel" data-slide-to="0" class="active"></li>
                            <?php else: ?>
                                <li data-target="#carousel" data-slide-to="<?= $count; ?>"></li>
                                <?php $count = $count + 1;
                            endif;
                        endforeach;
                        $count = 1 ?>
                    </ul>

                    <!-- The slideshow -->
                    <div class="carousel-inner" role="listbox">
                        <?php foreach ($experiences as $item => $experience):
                            if ($experience['id'] != 1): ?>
                                <div class="carousel-item">
                                    <a href="view/view.php?id=<?= $experience['id']; ?>" target=_blank>
                                        <img class="d-block w-100"
                                             src="<?= WEBROOT; ?>resources/image/exp/<?= $experience['image_name']; ?>"
                                             alt="<?= $experience['image_name']; ?>">
                                        <div class="carousel-caption legende">
                                            <h3 class="carousel-text"><?= $experience['name']; ?></h3>
                                        </div>
                                    </a>
                                </div>
                            <?php else: ?>
                                <div class="carousel-item active">
                                    <a href="view/view.php?id=<?= $experience['id']; ?>" target=_blank>
                                        <img class="d-block w-100"
                                             src="<?= WEBROOT; ?>resources/image/exp/<?= $experience['image_name']; ?>"
                                             alt="<?= $experience['image_name']; ?>">
                                        <div class="carousel-caption legende-active">
                                            <h3 class="carousel-text"><?= $experience['name']; ?></h3>
                                        </div>
                                    </a>
                                </div>
                            <?php endif; endforeach; ?>
                    </div>

                    <!-- Left and right controls -->
                    <a class="carousel-control-prev" href="#carousel" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                    </a>
                    <a class="carousel-control-next" href="#carousel" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                    </a>
                </div>

            </div>
        </div>

        <!- Section Expériences ->

        <div id="section4" class="container-fluid black">
            <h1 style="text-align: center" class="white-text">Expériences</h1>
            <p>&nbsp;</p>
            <p>&nbsp;</p>

            <div class="row mt-1">
                <div class="col-md-12">

                    <!-- Stepers Wrapper -->
                    <ul class="stepper stepper-vertical" style="margin-right: 0">

                        <?php foreach ($internships as $item => $internship): ?>
                            <li>
                                <a>
                                    <span class="circle white black-text"><?= $count; ?></span>
                                    <span class="label white-text">
                                        <?= $internship['date_debut']; ?> - <?= $internship['date_fin']; ?>
                                    </span>
                                </a>
                                <div class="step-content white black-text lighten-3 rounded">
                                    <h3><?= $internship['name']; ?></h3>
                                    <P><?= $internship['content']; ?></P>
                                </div>
                            </li>
                            <?php $count = $count + 1;
                        endforeach;
                        $count = 1 ?>

                    </ul>
                    <!-- /.Stepers Wrapper -->

                </div>
            </div>
        </div>

        <!- Section Formation ->

        <div id="section5" class="container-fluid">
            <h1 style="text-align: center">Formation</h1>
            <p>&nbsp;</p>
            <p>&nbsp;</p>

            <div class="row mt-1">
                <div class="col-md-12">

                    <ul class="stepper stepper-vertical" style="float: right; margin-right: 0">

                        <?php foreach ($degrees as $item => $degree): ?>
                            <li>
                                <a>
                                    <span class="circle black white-text"><?= $count; ?></span>
                                    <span class="label black-text">
                                        <?= $degree['annee']; ?>
                                    </span>
                                </a>
                                <div class="step-content black white-text lighten-3 rounded">
                                    <h3><?= $degree['name']; ?> - <?= $degree['options']; ?></h3>
                                    <p><?= $degree['adresse']; ?></p>
                                </div>
                            </li>
                            <?php $count = $count + 1;
                        endforeach;
                        $count = 1 ?>

                    </ul>

                </div>
            </div>
        </div>

        <!- Section Contact ->

        <div id="section6" class="container-fluid black">
            <h1 style="text-align: center" class="white-text">Contact</h1>
            <p>&nbsp;</p>
            <p>&nbsp;</p>
            <div class="container">
                <section class="mb-4">
                    <p class="text-center w-responsive mx-auto mb-5">Avez-vous une question ? Contactez-moi directement
                        !</p>
                    <div class="row">
                        <div class="col-md-9 mb-md-0 mb-5">
                            <form action="#" method="POST">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="md-form mb-0">
                                            <label for="name"></label><input type="text" id="name" name="name"
                                                                             class="form-control"
                                                                             placeholder="Votre nom" required>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="md-form mb-0">
                                            <label for="email"></label><input type="text" id="email" name="email"
                                                                              class="form-control"
                                                                              placeholder="Votre email" required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="md-form mb-0">
                                            <label for="subject"></label><input type="text" id="subject" name="subject"
                                                                                class="form-control"
                                                                                placeholder="Sujet"
                                                                                required>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="md-form">
                                            <label for="message"></label><textarea type="text" id="message"
                                                                                   name="message" rows="2"
                                                                                   class="form-control"
                                                                                   placeholder="Votre message"
                                                                                   required></textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="text-center text-md-left">
                                    <button type="submit" class="btn btn-outline-light" style="margin-top: 10px">Envoyer
                                        !
                                    </button>
                                </div>
                            </form>
                        </div>
                        <div class="col-md-3 text-center">
                            <ul class="list-unstyled mb-0">
                                <li><i class="fas fa-map-marker-alt fa-2x"></i>
                                    <p><a href="https://www.google.fr/maps/@48.1089188,-1.6603024,13z" class="contact"
                                          target="_blank">Rennes, Bretagne, France</a></p>
                                </li>
                                <li><i class="fas fa-phone mt-4 fa-2x"></i>
                                    <p><a href="tel:+33641280508" class="contact">06 41 28 05 08</a></p>
                                </li>
                                <li><i class="fas fa-envelope mt-4 fa-2x"></i>
                                    <p><a href="mailto:orgevalthomas@gmail.com"
                                          class="contact">orgevalthomas@gmail.com</a></p>
                                </li>
                            </ul>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>

    <script>
        $('.carousel').carousel({
            touch: true // default
        })
    </script>

<?php include "./view/templates/footer.php";
